package pig;

public class SevenPointPlayer extends Player {
	
	public boolean throwAgain ( Player opponent ) {
		// this player will hold after earning 7 or more points in a turn
		if (super.turnTotal() < 7) {
			return true;
		} else {
			return false;
		}
	}
	
}
