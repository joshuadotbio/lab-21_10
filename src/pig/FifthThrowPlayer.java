package pig;

public class FifthThrowPlayer extends Player {

	public boolean throwAgain ( Player opponent ) {
		// this player will always hold on the after the fifth throw
		if (super.rollCount() < 5) {
			return true;
		} else {
			return false;
		}
	}
}
