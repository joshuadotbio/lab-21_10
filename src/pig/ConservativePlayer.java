package pig;

public class ConservativePlayer extends Player {
	
	public boolean throwAgain ( Player opponent ) {
		// this player only throws once each turn
		return false;
	}
	
}
